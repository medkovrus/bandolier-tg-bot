import https from 'https';
import fs from 'fs';
import TelegramBot from 'node-telegram-bot-api';
import express from 'express';
import cors from 'cors';
import fetch from 'node-fetch';

const privateKey = fs.readFileSync('private.key', 'utf8');
const certificate = fs.readFileSync('certificate.crt', 'utf8');
const credentials = {key: privateKey, cert: certificate};
const token = process.env.BOT_API_KEY;
const adminChatId = process.env.adminChatId;
const adminBotToken = process.env.adminBotToken;
const webAppUrl = 'https://playful-unicorn-3f690d.netlify.app';
const bot = new TelegramBot(token, {polling: true});
const app = express()

app.use(express.json());
app.use(cors({
    origin: 'https://playful-unicorn-3f690d.netlify.app',
    methods: 'GET,POST,OPTIONS',
    allowedHeaders: 'DNT,User-Agent,X-Requested-With,If-Modified-Since,Cache-Control,Content-Type,Range',
    credentials: true,
    exposedHeaders: 'Content-Length,Content-Range',
}));


let chatArr = [];

bot.on('message', async (msg) => {
    const chatId = msg.chat.id;
    const text = msg.text;

    if (text === '/start') {
        if (!chatArr.includes(chatId)) {
            chatArr.push(chatId);
        } else {
            console.log(`chatId ${chatId} уже есть в массиве.`);
        }

        await bot.sendMessage(chatId, 'Добро пожаловать в магазин чехлов Bandolier 💞', {
            reply_markup: {
                inline_keyboard: [
                    [{text: 'Войти в магазин', web_app: {url: webAppUrl}}]
                ]
            }
        });
    }
    if (msg?.web_app_data?.data) {
        try {
            const data = JSON.parse(msg?.web_app_data?.data)
            const message = `Спасибо за ваш заказ ✅\nГород: ${data?.city}\nИмя: ${data?.name}\nТелефон: ${data?.phone}\nПочта: ${data?.email}`;
            await bot.sendMessage(chatId, message)
            await fetch(`https://api.telegram.org/bot${adminBotToken}/sendMessage`, {
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({
                    chat_id: adminChatId,
                    text: `Новая заявка:\n${data?.city}\n${data?.name}\n${data?.phone}\n${data?.email}\nid:${chatId}`
                })
            });
            setTimeout(async () => {
                await bot.sendMessage(chatId, 'С вами свяжутся в ближайшее время для уточнения заказа');
            }, 2000)
        } catch (e) {
            console.error(e);
        }
    }
});

app.post('/web-data', async (req, res) => {
    const {queryId, products = [], totalPrice} = req.body
    const message = `Ваша заявка на сумму ${totalPrice} ₽ - ${products.map(item => item.title).join(', ')} отправлена в обработку!`;
    try {
        await bot.answerWebAppQuery(queryId, {
            type: 'article',
            id: queryId,
            title: 'Заявка отправлена',
            input_message_content: {message_text: message}
        });
        const chatId = chatArr.join('')
        if (chatId) {
            await bot.sendMessage(chatId, 'Чтобы оформить заказ, заполните форму под клавиатурой 👇', {
                reply_markup: {
                    keyboard: [
                        [{text: 'Заполнить форму', web_app: {url: webAppUrl + '/form'}}]
                    ]
                }
            });
        }
        await fetch(`https://api.telegram.org/bot${adminBotToken}/sendMessage`, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json'
            },
            body: JSON.stringify({
                chat_id: adminChatId,
                text: message
            })
        });

        return res.status(200).json({})
    } catch (e) {
        return res.status(500).json({})
    }
})

const httpsServer = https.createServer(credentials, app);
httpsServer.listen(443, () => {
    console.log('HTTPS сервер запущен на порту 443');
});


